""" dePG: a helper module for the decryption of PGP files in a Project
"""  # __author__ = 'mlc'

import logging
import glob
import pgp
import getpass


logging.basicConfig(level=logging.INFO)


def decrypt(password=None):
    """
    Decrypt all files of with a .pgp extension in the current directory, given
    a password.
    | password: Password
    |   Type: String
    """

    password = getpass.getpass()
    pattern = '*.pgp'
    files = glob.glob(pattern)
    ext = pattern.strip('*')
    logging.info("%s %s files found:", len(files), pattern)

    # Handle Decryption -------------------------------------------------------
    if not len(files) <= 0:

        for i, infile in enumerate(files):
            logging.info("Decrypting file: %s", infile)
            infile = str(files[i])
            outfile = infile.rstrip(ext)
            pgp.decrypt_file(infile, outfile, password)
        else:
            return logging.info("Decryption complete! exiting...")


if __name__ == '__Main__':
    logging.info("Executed %s as a script.", 'dePG')
