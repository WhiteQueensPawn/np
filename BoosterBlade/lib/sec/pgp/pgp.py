""" pgpMOD: a helper module for the inclusion of PGP code in a Project
"""  # based on Ben Stroud's code

import logging
import subprocess


def decrypt_file(infile, outfile, phrase):
    """
    | infile: File to decrypt
    | outfile: Write decrypted file here
    | phrase: Password
    """
    args = ['gpg.exe', '--passphrase-fd', '0', '--yes', '--batch',
            '--skip-verify', '--quiet', '--always-trust', '--decrypt',
            '--output', outfile, infile]
    try:
        proc = subprocess.Popen(args, stdin=subprocess.PIPE)
        pipe = proc.stdin
        print >> pipe, phrase
        proc.communicate()
        retcode = proc.returncode
        logging.debug("Decryption Return Code %d" % retcode)
        if retcode != 0:
            raise Exception("Decryption failed with nonzero status")
    except OSError, e:
        logging.warn("Fatal: Decryption Execution Failed...")
        logging.warn(e)
        raise